var print = document.getElementById('print');
var erase = false;
var themes = [
    {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
];

localStorage.setItem('themes', JSON.stringify(themes));

var retrievedObject = localStorage.getItem('themes');
var retrievedTheme = localStorage.getItem('newSelected');

var go = function(x) {
	if (x === 'ac') {
	/* implemetnasi clear all */
	print.value = '';
	} else if (x === 'eval') {
		print.value = Math.round(evil(print.value) * 10000) / 10000;
		erase = true;
	} else {
		print.value += x;
	}
};

function evil(fn) {
	return new Function('return ' + fn)();
}


$(document).ready(function() {
	$("#img-min").click(function() {
		$(".chat-body").slideToggle();
		var src = ($(this).attr('src') === 'https://maxcdn.icons8.com/windows10/PNG/16/Arrows/angle_down-16.png')?
		'https://maxcdn.icons8.com/windows10/PNG/16/Arrows/angle_up-16.png' :
		'https://maxcdn.icons8.com/windows10/PNG/16/Arrows/angle_down-16.png';
		$(this).attr('src', src);
	});

	$('textarea').keypress(function(e) {
		if (e.which == 13) {
			var msg = $('textarea').val();
			var old = $('.msg-insert').html();
			if (msg.length == 1) {
				alert("Message kosong");
			}
			else {
				$('.msg-insert').html(old + '<p class="msg-send">' + msg + '</p>')
				$('textarea').val("");
			}
		}
	});

	$('.my-select').select2({
		'data' : JSON.parse(retrievedObject)
	});

	var retrievedTheme = localStorage.getItem('newSelected');
    var appliedTheme = JSON.parse(retrievedTheme);
    $('body').css('background', appliedTheme.selected.bcgColor);
    $('body').css('color', appliedTheme.selected.fontColor);
});


$('.apply-button').on('click', function(){
    
    var id = $(".my-select").select2("val");
    

    data = $.parseJSON(retrievedObject);
    $.each(data, function(i, item) {
    
	    if(id == item.id){
	        var text = item.text;
	        var background = item.bcgColor;
	        var font = item.fontColor;

	        $('body').css("background-color",background);
	        $('body').css("color", font);

	        var tes = {"selected":{"bcgColor":background,"fontColor":font, "text":text}};
	        
	        localStorage.setItem('newSelected', JSON.stringify(tes));
	    }
    });
});
